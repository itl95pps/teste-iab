import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ModalController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(
    public navCtrl: NavController,
    private iab: InAppBrowser,
    public modalCtrl: ModalController,
  ) {

  }

  ionViewWillEnter() {
    this.openIAB();
  }

  openIAB() {
    const browser = this.iab.create('https://ezdevs.com.br/', '_blank');

    browser.on('loadstop').subscribe(async (event) => {
      console.log('loadstop', event);
      if (event.url.indexOf('cases-de-sucesso') != -1) {
        await this.presentModal();
        browser.close();
      }

      if (event.url.indexOf('orcamento') != -1) {
        browser.close();
        this.navCtrl.setRoot('MenuPage');
      }
    });

    browser.show();
  }

  async presentModal() {
    const modal = this.modalCtrl.create('ConfigurationPage');
    await modal.present();
    modal.onDidDismiss((data) => {
      this.openIAB();
    });
  }
}
